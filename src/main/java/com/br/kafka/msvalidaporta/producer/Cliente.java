package com.br.kafka.msvalidaporta.producer;

public class Cliente {

    private String cliente;
    private long porta;
    private boolean temAcessoPorta;

    public Cliente() {
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public long getPorta() {
        return porta;
    }

    public void setPorta(long porta) {
        this.porta = porta;
    }

    public boolean isTemAcessoPorta() {
        return temAcessoPorta;
    }

    public void setTemAcessoPorta(boolean temAcessoPorta) {
        this.temAcessoPorta = temAcessoPorta;
    }
}
