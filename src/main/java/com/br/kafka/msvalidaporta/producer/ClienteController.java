package com.br.kafka.msvalidaporta.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClienteController {
    @Autowired
    private ClienteService clienteService;

    @PostMapping("/acesso/{cliente}/{porta}")
    public void create(@PathVariable String cliente, @PathVariable long porta) {
        boolean temAcessoPorta = clienteService.verificarAcessoPorta();

        Cliente clienteObjeto = new Cliente();
        clienteObjeto.setCliente(cliente);
        clienteObjeto.setPorta(porta);
        clienteObjeto.setTemAcessoPorta(temAcessoPorta);

        clienteService.enviarAoKafka(clienteObjeto);
    }
}
