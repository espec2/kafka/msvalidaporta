package com.br.kafka.msvalidaporta.producer;

import com.br.kafka.msvalidaporta.producer.Cliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class ClienteService {

    @Autowired
    private KafkaTemplate<String, Cliente> producer;

    public boolean verificarAcessoPorta(){
        Random random = new Random();
        return random.nextBoolean();
    }

    public void enviarAoKafka(Cliente cliente) {
        producer.send("spec2-andre-vinicius-1", cliente);
    }
}
