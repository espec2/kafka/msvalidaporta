package com.br.kafka.msvalidaporta.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsvalidaportaApplication {

	public static void main(String[] args) {
		SpringApplication.run(MsvalidaportaApplication.class, args);
	}

}
